package com.tmovie.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tmovie.model.MigratedVideoPage;

public interface MigratedVideoPageRepo extends JpaRepository<MigratedVideoPage, Integer> {

}
