package com.tmovie.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.tmovie.model.VideoPage;

public interface VideoPageRepo extends JpaRepository<VideoPage, Integer> {
    
    @Query("select vp from VideoPage vp where vp.migrated = false")
    public Page<VideoPage> findAllNonMigratedVideos(Pageable pageable);
    
    @Query("select count(vp.id) from VideoPage vp where vp.migrated = false")
    public Integer countAllNonMigratedVideos();
}
