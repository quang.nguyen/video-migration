package com.tmovie;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LinkMigrationApplication {

	public static void main(String[] args) {
		SpringApplication.run(LinkMigrationApplication.class, args);
	}
}
