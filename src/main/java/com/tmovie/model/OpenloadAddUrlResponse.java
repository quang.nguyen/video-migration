package com.tmovie.model;

public class OpenloadAddUrlResponse extends BaseResponse{
    public OpenloadAddUrlResult result;
    
    public static class OpenloadAddUrlResult {
        public String id;
        public String folderid;
    }
}
