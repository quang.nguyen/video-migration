package com.tmovie.model;

import java.util.Map;

public class OpenloadCheckRemoteUploadResponse extends BaseResponse{
    public Map<String, OpenloadCheckRemoteUploadResult> result;
    
    public static class OpenloadCheckRemoteUploadResult{
        public String id;
        public String remoteurl;
        public String status;
        public String bytes_loaded;
        public String bytes_total;
        public String folderid;
        public String added;
        public String last_update;
        public String extid;
        public String url;
    }
}
