package com.tmovie.service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.tmovie.model.MigratedVideoPage;
import com.tmovie.model.VideoPage;
import com.tmovie.repository.MigratedVideoPageRepo;
import com.tmovie.repository.VideoPageRepo;

@Service
public class DatabaseModelService {
    
    private MigratedVideoPageRepo newVideoRepo;
    private VideoPageRepo oldVideoRepo;
    private final OpenloadService openloadService;

    
    @Value("${com.tmovie.numOfVideos}")
    private int numOfMigration;
    
    private ScheduledExecutorService es;
    
    @Autowired
    public DatabaseModelService(MigratedVideoPageRepo newVideoRepo, VideoPageRepo oldVideoRepo, OpenloadService openloadService) {
        super();
        this.newVideoRepo = newVideoRepo;
        this.oldVideoRepo = oldVideoRepo;
        this.openloadService = openloadService;
        this.es = Executors.newScheduledThreadPool(200);
    }
    
    public List<MigratedVideoPage> migrateVideos(Pageable pageable) {
        Page<VideoPage> videoPages = oldVideoRepo.findAllNonMigratedVideos(pageable);
        List<MigratedVideoPage> migratedVideos = new ArrayList<>();
        videoPages.forEach(oldVideo -> {
            System.out.println(oldVideo.getVideoLink());
            String videoLink = this.openloadService.get(oldVideo.getVideoLink());
            if(videoLink != null) {
                oldVideo.setMigrated(true);
                oldVideoRepo.saveAndFlush(oldVideo);
                MigratedVideoPage newVideo = new MigratedVideoPage(oldVideo, videoLink);
                newVideo = newVideoRepo.saveAndFlush(newVideo);
                migratedVideos.add(newVideo);
                System.out.println(newVideo.getVideoLink());
            }
        });
        return migratedVideos;
    }
    
    private boolean hasNonMigratedVideos() {
       return this.oldVideoRepo.countAllNonMigratedVideos() > 0;
    }
    
    public void autoMigrate() {
        this.es.submit(() -> {
            Pageable pageable = new PageRequest(0, numOfMigration);
            while(this.hasNonMigratedVideos()) {
                this.migrateVideos(pageable);
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }
    
}
