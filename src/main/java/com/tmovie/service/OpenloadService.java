package com.tmovie.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.transform.Source;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.ResourceHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.converter.support.AllEncompassingFormHttpMessageConverter;
import org.springframework.http.converter.xml.SourceHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmovie.model.OpenloadAddUrlResponse;
import com.tmovie.model.OpenloadCheckRemoteUploadResponse;
import com.tmovie.model.OpenloadCheckRemoteUploadResponse.OpenloadCheckRemoteUploadResult;

@Service
public class OpenloadService {
    @Value("${com.tmovie.apiLogin}")
    private String apiLogin;
    
    @Value("${com.tmovie.apiKey}")
    private String apiKey;
    
    @Value("${com.tmovie.baseUrl}")
    private String baseUrl;
    
    @Value("${com.tmovie.migrationUri}")
    private String migrationUri;
    
    @Value("${com.tmovie.checkMigrationUri}")
    private String checkMigrationUri;
    private RestTemplate rest;
    private HttpHeaders headers;

    public OpenloadService() {
        List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
        messageConverters.add(new ByteArrayHttpMessageConverter());
        messageConverters.add(new StringHttpMessageConverter());
        messageConverters.add(new ResourceHttpMessageConverter());
        messageConverters.add(new SourceHttpMessageConverter<Source>());
        messageConverters.add(new AllEncompassingFormHttpMessageConverter());
        MappingJackson2HttpMessageConverter jsonConverter = new MappingJackson2HttpMessageConverter(new ObjectMapper());
        jsonConverter.setSupportedMediaTypes(Arrays.asList(new MediaType("application", "x-json")));
        messageConverters.add(jsonConverter);
        this.rest = new RestTemplate(messageConverters);
        this.headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(new MediaType("application", "x-json")));
        headers.setContentType(new MediaType("application", "x-json"));
    }
    
    public String get(String url) {
        try {
            String firstQuery = this.baseUrl + this.migrationUri;
            UriComponentsBuilder firstBuilder = UriComponentsBuilder.fromHttpUrl(firstQuery)
                    .queryParam("login", apiLogin)
                    .queryParam("key", apiKey)
                    .queryParam("url", url);
            HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
            ResponseEntity<OpenloadAddUrlResponse> firstResponseEntity = rest.exchange(firstBuilder.build().encode().toUri(),HttpMethod.GET, entity,OpenloadAddUrlResponse.class);
            OpenloadAddUrlResponse firstResponse = firstResponseEntity.getBody();
            if(firstResponse.result == null) return null;
            String fileId = firstResponse.result.id;
            if(fileId == null || fileId.isEmpty()) return null;
            
            
            String secondQuery = this.baseUrl + this.checkMigrationUri;
            UriComponentsBuilder secondBuilder = UriComponentsBuilder.fromHttpUrl(secondQuery)
                    .queryParam("login", apiLogin)
                    .queryParam("key", apiKey)
                    .queryParam("url", url);
            ResponseEntity<OpenloadCheckRemoteUploadResponse> secondResponseEntity = rest.exchange(secondBuilder.build().encode().toUri(), HttpMethod.GET, entity,OpenloadCheckRemoteUploadResponse.class);
            OpenloadCheckRemoteUploadResponse secondResponse = secondResponseEntity.getBody();
            OpenloadCheckRemoteUploadResult fileInfo = secondResponse.result.get(fileId);
            if(fileInfo == null) return null;
            String myUrl = fileInfo.url;
            if(myUrl == null || myUrl.isEmpty()) return null;
            return myUrl;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    } 
    
}
