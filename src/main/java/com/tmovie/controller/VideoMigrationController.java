package com.tmovie.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.tmovie.model.MigratedVideoPage;
import com.tmovie.service.DatabaseModelService;

@RestController
public class VideoMigrationController {
    private final DatabaseModelService modelService;

    @Autowired
    public VideoMigrationController(DatabaseModelService modelService) {
        super();
        this.modelService = modelService;
    }
    
    @GetMapping(value="/migrate")
    @ResponseBody ResponseEntity<List<MigratedVideoPage>> migrate(Pageable pageable){
        List<MigratedVideoPage> newVideos = this.modelService.migrateVideos(pageable);
        return new ResponseEntity<List<MigratedVideoPage>>(newVideos, HttpStatus.OK);
    }
    
    @GetMapping(value="/auto-migrate")
    @ResponseBody ResponseEntity<String> migrate(){
        this.modelService.autoMigrate();
        return new ResponseEntity<String>("Video migration task is executing.", HttpStatus.OK);
    }

}
